import { Component, OnInit } from '@angular/core';
import { Movie } from '../../../interfaces/movie.model';
import { Observable } from 'rxjs';
import { AppState } from '../../../state/app.state';
import { Store } from '@ngrx/store';
import { selectMoviesList } from '../../../state/selectors/movies.selectors';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss'],
})
export class MoviesListComponent implements OnInit {
  public movies$: Observable<Array<Movie>> = new Observable();
  constructor(private readonly store: Store<AppState>) {}

  ngOnInit(): void {
    this.movies$ = this.store.select(selectMoviesList);
  }
}
