import { createAction, props } from '@ngrx/store';
import { Movie } from '../../interfaces/movie.model';

export const loadMovies = createAction('[Movies List] Loading Movies');

export const loadedMovies = createAction(
  '[Movies List] Loaded Movies',
  props<{ items: Array<Movie> }>()
);
