import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from '../../state/app.state';
import { loadMovies } from '../../state/actions/movies.actions';
import { selectMoviesLoading } from '../../state/selectors/movies.selectors';

@Component({
  selector: 'app-billboard',
  templateUrl: './billboard.component.html',
  styleUrls: ['./billboard.component.scss'],
})
export class BillboardComponent implements OnInit {
  public isLoading$: Observable<boolean> = new Observable();

  constructor(private readonly store: Store<AppState>) {}

  ngOnInit(): void {
    this.isLoading$ = this.store.select(selectMoviesLoading);

    this.store.dispatch(loadMovies());
  }
}
