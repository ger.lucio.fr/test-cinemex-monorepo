import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillboardComponent } from './billboard.component';
import { LoaderModule } from '@cinemex/commons';
import { MoviesListModule } from './movies-list/movies-list.module';

@NgModule({
  declarations: [BillboardComponent],
  imports: [CommonModule, LoaderModule, MoviesListModule],
  exports: [BillboardComponent],
})
export class BillboardModule {}
