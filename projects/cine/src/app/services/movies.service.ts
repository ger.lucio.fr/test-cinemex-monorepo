import { Injectable } from '@angular/core';
import { Observable, of, delay } from 'rxjs';
import { Movie } from '../interfaces';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {
  public movies: Array<Movie> = [
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'John Wick 4',
      category: ['premiere'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/hVsoohj61iPUe91-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'Super Mario Bros. La Película',
      category: ['presale'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/iDLT9GCTG19AGMM-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'John Wick 4',
      category: ['premiere'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/hVsoohj61iPUe91-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'Super Mario Bros. La Película',
      category: ['presale'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/iDLT9GCTG19AGMM-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'John Wick 4',
      category: ['premiere'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/hVsoohj61iPUe91-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'Super Mario Bros. La Película',
      category: ['presale'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/iDLT9GCTG19AGMM-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'John Wick 4',
      category: ['premiere'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/hVsoohj61iPUe91-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'Super Mario Bros. La Película',
      category: ['presale'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/iDLT9GCTG19AGMM-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'John Wick 4',
      category: ['premiere'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/hVsoohj61iPUe91-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'Super Mario Bros. La Película',
      category: ['presale'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/iDLT9GCTG19AGMM-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'John Wick 4',
      category: ['premiere'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/hVsoohj61iPUe91-360x540.jpg',
    },
    {
      id: 'ge34y3-3h4h34-3h34h',
      name: 'Super Mario Bros. La Película',
      category: ['presale'],
      img: 'https://s3.amazonaws.com/statics3.cinemex.com/movie_posters/iDLT9GCTG19AGMM-360x540.jpg',
    },
  ];
  constructor() {}

  public getMovies(): Observable<{ items: Array<Movie> }> {
    return of({ items: this.movies }).pipe(delay(1000));
  }
}
