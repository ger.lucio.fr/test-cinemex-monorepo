import { createSelector } from '@ngrx/store';
import { MovieState } from '../../interfaces/movie.model';
import { AppState } from '../app.state';

export const selectMovies = (state: AppState) => state.movies;

export const selectMoviesList = createSelector(
  selectMovies,
  (state: MovieState) => state.items
);

export const selectMoviesLoading = createSelector(
  selectMovies,
  (state: MovieState) => state.isLoading
);
