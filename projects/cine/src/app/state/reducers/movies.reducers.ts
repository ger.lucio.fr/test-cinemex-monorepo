import { createReducer, on } from '@ngrx/store';
import { MovieState } from '../../interfaces/movie.model';
import { loadedMovies, loadMovies } from '../actions/movies.actions';

export const initialState: MovieState = { isLoading: false, items: [] };

export const moviesReducer = createReducer(
  initialState,

  on(loadMovies, (state) => {
    return { ...state, isLoading: true };
  }),
  on(loadedMovies, (state, { items }) => {
    return { ...state, isLoading: false, items };
  })
);
