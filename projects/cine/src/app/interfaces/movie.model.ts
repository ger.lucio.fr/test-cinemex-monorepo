export interface Movie {
  id: string;
  name: string;
  category: Array<string>;
  img: string;
}

export interface MovieState {
  isLoading: boolean;
  items: Array<Movie>;
}
