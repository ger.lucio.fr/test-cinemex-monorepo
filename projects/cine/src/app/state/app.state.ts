import { ActionReducerMap } from '@ngrx/store';
import { MovieState } from '../interfaces/movie.model';
import { moviesReducer } from './reducers/movies.reducers';

export interface AppState {
  movies: MovieState;
}

export const ROOT_REDUCERS: ActionReducerMap<AppState> = {
  movies: moviesReducer,
};
