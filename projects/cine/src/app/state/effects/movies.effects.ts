import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MoviesService } from '../../services';
import { exhaustMap, map, catchError } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

@Injectable()
export class MoviesEffects {
  loadMovies$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Movies List] Loading Movies'),
      exhaustMap(() =>
        this.moviesService.getMovies().pipe(
          map(({ items }) => ({
            type: '[Movies List] Loaded Movies',
            items,
          })),
          catchError(() => EMPTY)
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private readonly moviesService: MoviesService
  ) {}
}
